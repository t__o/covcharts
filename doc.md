


# Changelog 

v0.04 Fix reset
v0.03 maths functions
v0.02 sync x axis
v0.01 repack, data auto loading


### Project dataviz d3js vue 

https://medialab.sciencespo.fr/actu/coronavirus-country-comparator/
https://boogheta.github.io/coronavirus-countries/#deceased&places=China,France,Iran,Italy,Spain,USA,United%20Kingdom&alignTo=deceased
https://github.com/boogheta/coronavirus-countries/blob/master/js/corona.js


### vuejs d3

Workshop : 
Composing D3.js Visuals with Vue.js Workshop https://www.youtube.com/watch?v=CkFktv0p3pw
Visualizing Hierarchies with Vue.js and D3.js  https://www.youtube.com/watch?v=GuecoTcqmVE

Bar with animation : 
https://codepen.io/callumacrae/pen/oyXXWR