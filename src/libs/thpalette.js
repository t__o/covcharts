/*
*
*  Hacked from : http://google.github.io/palette.js/
*
*
* */
let clamp = (v) => {
    return v > 0 ? (v < 1 ? v : 1) : 0;
}

let rgb = (r, g, b) => {
    return [r, g, b].map(function (v) {
        v = Number(Math.round(clamp(v) * 255)).toString(16);
        return v.length == 1 ? '0' + v : v;
    }).join('');
}

let poly = function (x, varargs) {
    varargs
    var i = arguments.length - 1, n = arguments[i];
    while (i > 1) {
        n = n * x + arguments[--i];
    }
    return n;
}

let erf = function (x) {
    // https://en.wikipedia.org/wiki/Error_function#Approximation_with_elementary_functions
    // This produces a maximum error of 0.0005 which is more then we need.  In
    // the worst case, that error is multiplied by four and then divided by two
    // before being multiplied by 255, so in the end, the error is multiplied by
    // 510 which produces 0.255 which is less than a single colour step.
    var y = poly(Math.abs(x), 1, 0.278393, 0.230389, 0.000972, 0.078108);
    y *= y; // y^2
    y *= y; // y^4
    y = 1 - 1 / y;
    return x < 0 ? -y : y;
};
let rgbColor = function (r, g, b) {
    return [r, g, b].map(function (v) {
        v = Number(Math.round(clamp(v) * 255)).toString(16);
        return v.length === 1 ? '0' + v : v;
    }).join('');
};

let hsvColor = function (h, opt_s, opt_v) {
    h *= 6;
    var s = opt_s === void (0) ? 1 : clamp(opt_s);
    var v = opt_v === void (0) ? 1 : clamp(opt_v);
    var x = v * (1 - s * Math.abs(h % 2 - 1));
    var m = v * (1 - s);
    switch (Math.floor(h) % 6) {
        case 0:
            return rgbColor(v, x, m);
        case 1:
            return rgbColor(x, v, m);
        case 2:
            return rgbColor(m, v, x);
        case 3:
            return rgbColor(m, x, v);
        case 4:
            return rgbColor(x, m, v);
        default:
            return rgbColor(v, m, x);
    }
};


let palettes = {
    tolRainbowColor: (x) => {
        return rgb(poly(x, 0.472, -0.567, 4.05) / poly(x, 1, 8.72, -19.17, 14.1),
            poly(x, 0.108932, -1.22635, 27.284, -98.577, 163.3, -131.395,
                40.634),
            1 / poly(x, 1.97, 3.54, -68.5, 243, -297, 125));
    },
    tolDivergingColor: (x) => {
        var g = poly(x, 0.572, 1.524, -1.811) / poly(x, 1, -0.291, 0.1574);
        return rgb(poly(x, 0.235, -2.13, 26.92, -65.5, 63.5, -22.36),
            g * g,
            1 / poly(x, 1.579, -4.03, 12.92, -31.4, 48.6, -23.36));
    },
    tolSequentialColor: function (x) {
        return rgb(1 - 0.392 * (1 + erf((x - 0.869) / 0.255)),
            1.021 - 0.456 * (1 + erf((x - 0.527) / 0.376)),
            1 - 0.493 * (1 + erf((x - 0.272) / 0.309)));
    },
    hsv: x => hsvColor(x, 1, 1),
    hsvLight: x => hsvColor(x, .5, 1),
    hsvDark: x => hsvColor(x, 1, .5)
}

let getPalette = (type, count) => {
    let palette = []
    let factor = 1 / count

    for (let i = 0; i < count; i++) {
        let id = i * factor
        palette.push('#' + palettes[type](id))
    }
    return palette
}

let palettesList = ['tolRainbowColor',
    'tolDivergingColor',
    'tolSequentialColor',
    'hsv',
    'hsvLight',
    'hsvDark',]



export  {getPalette, palettesList}













