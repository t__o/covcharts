# CovCharts

Charts on COVID 19

Using vuejs, vuetify and d3js

[Demo](https://t__o.gitlab.io/covcharts)

Data source from : https://github.com/pomber/covid19
 
**Numbers are not reality**

## Features : 

- Maths functions : Exp, pow, Logistic
- sync X axis at a value
- reset 
- set a limit for y axis
- select first and last day
- select data source (confirmed, deaths, recovered)
- select scale (log/linear)
- select countries